﻿namespace GameOfLife.Windows
{
    partial class GameOfLifeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnManualCalculate = new System.Windows.Forms.Button();
            this.chkTorusSurface = new System.Windows.Forms.CheckBox();
            this.tmrAutoUpdate = new System.Windows.Forms.Timer(this.components);
            this.btnAutoUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblWidth = new System.Windows.Forms.Label();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.lblHeight = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.btnCreateUniverse = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this._golGridControl = new GameOfLife.Windows.Controls.GolGridControl();
            this.SuspendLayout();
            // 
            // btnManualCalculate
            // 
            this.btnManualCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManualCalculate.Location = new System.Drawing.Point(602, 153);
            this.btnManualCalculate.Name = "btnManualCalculate";
            this.btnManualCalculate.Size = new System.Drawing.Size(109, 23);
            this.btnManualCalculate.TabIndex = 1;
            this.btnManualCalculate.Text = "Update";
            this.btnManualCalculate.UseVisualStyleBackColor = true;
            this.btnManualCalculate.Click += new System.EventHandler(this.btnUpdateGrid_Click);
            // 
            // chkTorusSurface
            // 
            this.chkTorusSurface.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTorusSurface.AutoSize = true;
            this.chkTorusSurface.Checked = true;
            this.chkTorusSurface.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTorusSurface.Location = new System.Drawing.Point(604, 130);
            this.chkTorusSurface.Name = "chkTorusSurface";
            this.chkTorusSurface.Size = new System.Drawing.Size(91, 17);
            this.chkTorusSurface.TabIndex = 2;
            this.chkTorusSurface.Text = "Torus surface";
            this.chkTorusSurface.UseVisualStyleBackColor = true;
            this.chkTorusSurface.CheckedChanged += new System.EventHandler(this.chkTorusSurface_CheckedChanged);
            // 
            // tmrAutoUpdate
            // 
            this.tmrAutoUpdate.Tick += new System.EventHandler(this.tmrAutoUpdate_Tick);
            // 
            // btnAutoUpdate
            // 
            this.btnAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoUpdate.Location = new System.Drawing.Point(602, 182);
            this.btnAutoUpdate.Name = "btnAutoUpdate";
            this.btnAutoUpdate.Size = new System.Drawing.Size(109, 37);
            this.btnAutoUpdate.TabIndex = 3;
            this.btnAutoUpdate.Text = "Start auto update";
            this.btnAutoUpdate.UseVisualStyleBackColor = true;
            this.btnAutoUpdate.Click += new System.EventHandler(this.btnAutoUpdate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(602, 225);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(109, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblWidth
            // 
            this.lblWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWidth.AutoSize = true;
            this.lblWidth.Location = new System.Drawing.Point(603, 9);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(84, 13);
            this.lblWidth.TabIndex = 5;
            this.lblWidth.Text = "Width (max 100)";
            // 
            // txtWidth
            // 
            this.txtWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWidth.Location = new System.Drawing.Point(602, 25);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(109, 20);
            this.txtWidth.TabIndex = 6;
            // 
            // lblHeight
            // 
            this.lblHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeight.AutoSize = true;
            this.lblHeight.Location = new System.Drawing.Point(602, 52);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(87, 13);
            this.lblHeight.TabIndex = 7;
            this.lblHeight.Text = "Height (max 100)";
            // 
            // txtHeight
            // 
            this.txtHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHeight.Location = new System.Drawing.Point(602, 69);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(109, 20);
            this.txtHeight.TabIndex = 8;
            // 
            // btnCreateUniverse
            // 
            this.btnCreateUniverse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateUniverse.Location = new System.Drawing.Point(604, 96);
            this.btnCreateUniverse.Name = "btnCreateUniverse";
            this.btnCreateUniverse.Size = new System.Drawing.Size(107, 23);
            this.btnCreateUniverse.TabIndex = 9;
            this.btnCreateUniverse.Text = "Create universe";
            this.btnCreateUniverse.UseVisualStyleBackColor = true;
            this.btnCreateUniverse.Click += new System.EventHandler(this.btnCreateUniverse_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(602, 254);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(602, 283);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(109, 39);
            this.btnLoad.TabIndex = 11;
            this.btnLoad.Text = "Load last saved universe";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // _golGridControl
            // 
            this._golGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._golGridControl.CellHeight = 15;
            this._golGridControl.CellWidth = 15;
            this._golGridControl.Location = new System.Drawing.Point(3, 2);
            this._golGridControl.Name = "_golGridControl";
            this._golGridControl.Size = new System.Drawing.Size(594, 513);
            this._golGridControl.TabIndex = 0;
            this._golGridControl.TorusSurface = false;
            this._golGridControl.DrawCell += new System.EventHandler<GameOfLife.Windows.Events.DrawCellEventArgs>(this.gridControl_DrawCell);
            // 
            // GameOfLifeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 515);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCreateUniverse);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAutoUpdate);
            this.Controls.Add(this.chkTorusSurface);
            this.Controls.Add(this.btnManualCalculate);
            this.Controls.Add(this._golGridControl);
            this.MinimumSize = new System.Drawing.Size(330, 220);
            this.Name = "GameOfLifeForm";
            this.Text = "Game of Life";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.GolGridControl _golGridControl;
        private System.Windows.Forms.Button btnManualCalculate;
        private System.Windows.Forms.CheckBox chkTorusSurface;
        private System.Windows.Forms.Timer tmrAutoUpdate;
        private System.Windows.Forms.Button btnAutoUpdate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Button btnCreateUniverse;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
    }
}

