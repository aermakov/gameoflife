﻿using System;
using System.Drawing;
using GameOfLife.Core;

namespace GameOfLife.Windows.Events
{
    public class DrawCellEventArgs : EventArgs
    {
        public GolCell GolCell { get; set; }

        public Brush Brush { get; set; }
    }
}