﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GameOfLife.Core;
using GameOfLife.Core.Factories;
using GameOfLife.Core.Serialization;
using GameOfLife.Core.Storages;
using GameOfLife.Windows.Events;

namespace GameOfLife.Windows
{
    public partial class GameOfLifeForm : Form
    {
        #region Fields

        private const string GRID_NAME_FORMAT = "yyyy-M-d HH-mm tt";
        private const int MAX_GRID_WIDTH = 100;
        private const int MAX_GRID_HEIGHT = 100;

        private bool _autoUpdate;
        private bool _isUpdating;

        #endregion

        #region Ctors

        public GameOfLifeForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        private bool AutoUpdate
        {
            get { return _autoUpdate; }
            set
            {
                _autoUpdate = value;

                AutoUpdateText =
                    _autoUpdate
                        ? "Stop auto update"
                        : "Start auto update";
            }
        }

        private string AutoUpdateText
        {
            set { btnAutoUpdate.Text = value; }
        }

        #endregion

        #region Methods

        private void CreateUniverse()
        {
            ulong width;
            ulong height;
            if (UInt64.TryParse(txtWidth.Text, out width) &&
                UInt64.TryParse(txtHeight.Text, out height) &&
                width <= MAX_GRID_WIDTH &&
                height <= MAX_GRID_HEIGHT)
            {
                GolGrid grid = new GridFactory().CreateGrid(width, height);

                SetGrid(grid);
            }
            else
            {
                MessageBox.Show("Failed to create an universe");
            }
        }

        private void SetGrid(GolGrid grid)
        {
            txtHeight.Text = grid.Height.ToString(CultureInfo.InvariantCulture);
            txtWidth.Text = grid.Width.ToString(CultureInfo.InvariantCulture);

            _golGridControl.SetGrid(grid);
        }

        private void UpdateGrid()
        {
            if (_isUpdating) return;
            _isUpdating = true;

            IList<GolCell> updatedCells = _golGridControl.UpdateGrid();

            int updatedCellsCount = updatedCells.Count();
            int aliveCellsCount = _golGridControl.GetAliveCellsCount();

            string text = null;
            if (updatedCellsCount == 0)
            {
                text = "Stable configuration detected";
            }
            else if (aliveCellsCount == 0)
            {
                text = "All cells were died";
            }

            if (text != null)
            {
                AutoUpdate = false;
                tmrAutoUpdate.Enabled = false;

                MessageBox.Show(text);
            }

            _isUpdating = false;
        }

        #endregion

        #region Handlers

        private void btnCreateUniverse_Click(object sender, EventArgs e)
        {
            CreateUniverse();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveGrid();
        }

        private void SaveGrid()
        {
            IStorage storage = new StorageFactory().CreateStorage();
            IGolGridSerializer serializer = new JsonGolGridSerializer();
            GolGrid grid = _golGridControl.GetGrid();
            string name = DateTime.Now.ToString(GRID_NAME_FORMAT).Trim();
            storage.Save(serializer, grid, name);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadLastGrid();
        }

        private void LoadGrid(string name)
        {
            IStorage storage = new StorageFactory().CreateStorage();
            IGolGridSerializer serializer = new JsonGolGridSerializer();
            GolGrid grid = storage.Load(serializer, name);
            SetGrid(grid);
        }

        private void LoadLastGrid()
        {
            IStorage storage = new StorageFactory().CreateStorage();
            string lastName = storage.EnumerateNames()
                .OrderByDescending(x => DateTime.ParseExact(x, GRID_NAME_FORMAT, CultureInfo.InvariantCulture))
                .FirstOrDefault();
            if (!string.IsNullOrEmpty(lastName))
            {
                LoadGrid(lastName);
            }
        }

        private void btnUpdateGrid_Click(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void chkTorusSurface_CheckedChanged(object sender, EventArgs e)
        {
            _golGridControl.TorusSurface = chkTorusSurface.Checked;
        }

        private void btnAutoUpdate_Click(object sender, EventArgs e)
        {
            AutoUpdate = !AutoUpdate;
            tmrAutoUpdate.Enabled = AutoUpdate;
        }

        private void tmrAutoUpdate_Tick(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _golGridControl.Clear();
        }

        private void gridControl_DrawCell(object sender, DrawCellEventArgs e)
        {
            e.Brush = Brushes.DarkGreen;
        }

        #endregion
    }
}