﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GameOfLife.Core;
using GameOfLife.Windows.Events;

namespace GameOfLife.Windows.Controls
{
    public partial class GolGridControl : UserControl
    {
        #region Events

        public event EventHandler<DrawCellEventArgs> DrawCell;

        protected virtual void OnDrawCell(object sender, DrawCellEventArgs e)
        {
            if (DrawCell != null)
                DrawCell(sender, e);
        }

        #endregion

        #region Fields

        private bool _canUpdate = true;

        private GolGrid _grid;

        #endregion

        #region Ctors

        public GolGridControl()
        {
            InitializeComponent();

            Initialize();
        }

        #endregion

        #region Properties

        public int CellWidth { get; set; }

        public int CellHeight { get; set; }

        public bool TorusSurface
        {
            get { return _grid != null && _grid.TorusSurface; }
            set
            {
                if (_grid != null)
                {
                    _grid.TorusSurface = value;
                }
            }
        }

        #endregion

        #region Methods

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public void BeginUpdate()
        {
            _canUpdate = false;
        }

        public void EndUpdate()
        {
            _canUpdate = true;

            DrawInternal();
        }

        public int GetGenerationNumber()
        {
            return _grid.GenerationNumber;
        }

        public int GetAliveCellsCount()
        {
            if (_grid != null) return _grid.GetAliveCellsCount();
            return 0;
        }

        public IList<GolCell> UpdateGrid()
        {
            BeginUpdate();

            IList<GolCell> updatedCells = null;
            if (_grid != null)
            {
                updatedCells = _grid.Update();

                EndUpdate();
            }
            return updatedCells;
        }

        public void Clear()
        {
            BeginUpdate();

            if (_grid != null)
            {
                GolCell[] aliveCells = _grid.GetAliveCells().ToArray();
                foreach (GolCell cell in aliveCells)
                {
                    _grid.DieCell(cell, true);
                }
            }

            EndUpdate();
        }

        private void Initialize()
        {
            DoubleBuffered = true;

            SetStyle(ControlStyles.AllPaintingInWmPaint |
                     ControlStyles.UserPaint, true);

            UpdateStyles();

            UpdateCellDimensions();
        }

        private void UpdateCellDimensions()
        {
            if (_grid != null)
            {
                CellHeight = (int) Math.Floor(1d*Height/_grid.Height);
                CellWidth = (int) Math.Floor(1d*Width/_grid.Width);
                CellHeight = CellWidth = Math.Min(CellHeight, CellWidth);
            }
        }

        protected virtual void DrawInternal()
        {
            DrawInternal(Graphics.FromHwnd(Handle));
        }

        protected virtual void DrawInternal(Graphics g)
        {
            g.FillRectangle(Brushes.White, 0, 0, Width - 1, Height - 1);
            g.DrawRectangle(Pens.LightGray, 0, 0, Width - 1, Height - 1);

            if (_grid != null)
            {
                for (ulong i = 0; i <= _grid.Height; i++)
                {
                    g.DrawLine(Pens.LightGray, 0, i*(float) CellHeight,
                        _grid.Width*(float) CellWidth, i*(float) CellHeight);
                }

                for (ulong j = 0; j <= _grid.Width; j++)
                {
                    g.DrawLine(Pens.LightGray, j*(float) CellWidth, 0,
                        j*(float) CellWidth, _grid.Height*(float) CellHeight);
                }

                IEnumerable<GolCell> aliveCells = _grid.GetAliveCells();
                foreach (GolCell cell in aliveCells)
                {
                    var e = new DrawCellEventArgs
                            {
                                GolCell = cell,
                                Brush = Brushes.Black
                            };

                    OnDrawCell(this, e);

                    g.FillRectangle(e.Brush, cell.X*(float) CellWidth + 1,
                        cell.Y*(float) CellHeight + 1, CellWidth - 1, CellHeight - 1);
                }
            }
        }

        private GolCell GetCellAtPoint(Point pt)
        {
            GolCell cell = null;
            var x = (ulong) (pt.X/CellWidth);
            var y = (ulong) (pt.Y/CellHeight);
            if (_grid != null)
            {
                cell = _grid.GetCell(x, y);
            }
            return cell;
        }

        #endregion

        #region Handlers

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (_canUpdate)
            {
                DrawInternal(e.Graphics);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            UpdateCellDimensions();
            DrawInternal();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            GolCell cell = GetCellAtPoint(e.Location);
            if (cell != null && (_grid.Height > cell.Y && _grid.Width > cell.X))
            {
                if (cell.IsDead)
                {
                    _grid.RetCell(cell, true);
                }
                else
                {
                    _grid.DieCell(cell, true);
                }

                DrawInternal();
            }
        }

        #endregion

        public void SetGrid(GolGrid grid)
        {
            _grid = grid;

            UpdateCellDimensions();
            BeginUpdate();
            EndUpdate();
        }

        public GolGrid GetGrid()
        {
            return _grid;
        }
    }
}