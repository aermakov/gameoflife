﻿namespace GameOfLife.Core
{
    /// <summary>
    ///     Состояние клетки
    /// </summary>
    public enum GolCellState
    {
        /// <summary>
        ///     Мертвая
        /// </summary>
        Dead,

        /// <summary>
        ///     Живая
        /// </summary>
        Alive
    }
}