﻿using System.Collections.Generic;
using GameOfLife.Core.Storages;

namespace GameOfLife.Core.Factories
{
    public class StorageFactory : IStorageFactory
    {
        public IStorage CreateStorage()
        {
            IStorage storage = new FileSystemStorage();
            storage.Configure(new Dictionary<string, string>
                              {
                                  {FileSystemStorage.OUTPUT_DIRECTORY_KEY, "Data"}
                              });
            return storage;
        }
    }
}