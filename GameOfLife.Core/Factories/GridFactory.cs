﻿using System.Collections.Generic;

namespace GameOfLife.Core.Factories
{
    public class GridFactory : IGridFactory
    {
        public GolGrid CreateGrid(ulong width, ulong height)
        {
            return CreateGrid(width, height, new GolCell[] {});
        }

        public GolGrid CreateGrid(ulong width, ulong height, IEnumerable<GolCell> cells)
        {
            return new GolGrid(width, height, cells);
        }
    }
}