﻿using System.Collections.Generic;

namespace GameOfLife.Core.Factories
{
    public interface IGridFactory
    {
        GolGrid CreateGrid(ulong width, ulong height);
        GolGrid CreateGrid(ulong width, ulong height, IEnumerable<GolCell> cells);
    }
}