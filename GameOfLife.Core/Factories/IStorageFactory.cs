﻿using GameOfLife.Core.Storages;

namespace GameOfLife.Core.Factories
{
    public interface IStorageFactory
    {
        IStorage CreateStorage();
    }
}