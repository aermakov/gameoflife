﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameOfLife.Core
{
    [Serializable]
    public class GolGrid
    {
        #region Events

        #endregion

        #region Fields

        private readonly SparseMatrix<GolCell> _cellsMatrix = new SparseMatrix<GolCell>();

        #endregion

        #region Ctors

        protected internal GolGrid(ulong width, ulong height, IEnumerable<GolCell> aliveCells = null)
        {
            Width = width;
            Height = height;
            TorusSurface = true;

            if (aliveCells != null)
            {
                RetCells(aliveCells, true);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Возращает номер поколения
        /// </summary>
        public int GenerationNumber { get; private set; }

        /// <summary>
        ///     Возвращает ширину сетки
        /// </summary>
        public ulong Width { get; set; }

        /// <summary>
        ///     Возвращает высоту сетки
        /// </summary>
        public ulong Height { get; set; }

        /// <summary>
        ///     Возращает значение или устанавливает значение, показывающее,
        ///     что сетка эмулирует поверхность Тора ( http://ru.wikipedia.org/wiki/Тор_(поверхность) )
        /// </summary>
        public bool TorusSurface { get; set; }

        #endregion

        #region Methods

        public IEnumerator<GolCell> GetCellsEnumerator()
        {
            return _cellsMatrix.GetEnumerator();
        }

        public void RetCell(GolCell cell, bool applyUpdate = false)
        {
            if (cell.IsDead)
            {
                cell.Ret(applyUpdate);

                if (cell.IsAlive)
                {
                    AddCell(cell);
                }
            }
        }

        public void RetCell(ulong x, ulong y, bool applyUpdate = false)
        {
            GolCell cell = GetCell(x, y);

            RetCell(cell, applyUpdate);
        }

        public void DieCell(GolCell cell, bool applyUpdate = false)
        {
            if (cell.IsAlive)
            {
                cell.Die(applyUpdate);

                if (cell.IsDead)
                {
                    RemoveCell(cell);
                }
            }
        }

        public void DieCell(ulong x, ulong y, bool applyUpdate = false)
        {
            GolCell cell = GetCell(x, y);

            DieCell(cell, applyUpdate);
        }

        public void RetCells(IEnumerator<GolCell> cells, bool applyUpdate = false)
        {
            while (cells.MoveNext())
            {
                GolCell cell = cells.Current;

                RetCell(cell.X, cell.Y, applyUpdate);
            }
        }

        public void RetCells(IEnumerable<GolCell> cells, bool applyUpdate = false)
        {
            foreach (GolCell cell in cells)
            {
                RetCell(cell.X, cell.Y, applyUpdate);
            }
        }

        public IEnumerable<GolCell> GetAliveCells()
        {
            IEnumerator<GolCell> enumerator = GetCellsEnumerator();
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current;
            }
        }

        public int GetAliveCellsCount()
        {
            return _cellsMatrix.Count();
        }

        public GolCell GetCell(ulong x, ulong y)
        {
            GolCell cell = _cellsMatrix.Get(x, y);
            if (cell == null)
            {
                if (CheckCellInBounds(x, y))
                {
                    cell = new GolCell(x, y, GolCellState.Dead);
                }
            }

            return cell;
        }

        private bool CheckCellInBounds(ulong x, ulong y)
        {
            return y >= 0 && x >= 0 && y < Height && x < Width;
        }

        public GolCell GetNeighborCell(ulong x, ulong y)
        {
            if (TorusSurface)
            {
                if (y == (decimal) -1)
                {
                    y = Height - 1;
                }
                else if (y == Height)
                {
                    y = 0;
                }

                if (x == (decimal) -1)
                {
                    x = Width - 1;
                }
                else if (x == Width)
                {
                    x = 0;
                }
            }

            return GetCell(x, y);
        }

        public void ApplyUpdate(IEnumerable<GolCell> cells)
        {
            foreach (GolCell cell in cells)
            {
                cell.State = cell.NextGenerationState;
                if (cell.IsAlive)
                {
                    AddCell(cell);
                }
                else
                {
                    RemoveCell(cell);
                }
            }
        }

        public IList<GolCell> Update(bool applyUpdate = true)
        {
            var cellsSet = new HashSet<GolCell>();
            IList<GolCell> updatedCells = new List<GolCell>();
            foreach (GolCell cell in GetAliveCells())
            {
                cellsSet.Add(cell);

                IEnumerable<GolCell> neighbors = GetNeighbors(cell);
                foreach (GolCell neighbor in neighbors)
                {
                    cellsSet.Add(neighbor);
                }
            }

            foreach (GolCell cell in cellsSet)
            {
                IEnumerable<GolCell> neighbors = GetNeighbors(cell);
                int aliveNeighborsCount = neighbors.Count(x => x.IsAlive);
                if (cell.IsDead)
                {
                    if (aliveNeighborsCount == 3)
                    {
                        RetCell(cell);
                        updatedCells.Add(cell);
                    }
                }
                else
                {
                    if (aliveNeighborsCount < 2 || aliveNeighborsCount > 3)
                    {
                        DieCell(cell);
                        updatedCells.Add(cell);
                    }
                }
            }

            IncrementGenerationNumber();

            if (applyUpdate)
            {
                ApplyUpdate(updatedCells);
            }
            return updatedCells;
        }

        private void RemoveCell(GolCell cell)
        {
            _cellsMatrix.Remove(cell.X, cell.Y);
        }

        private void AddCell(GolCell cell)
        {
            _cellsMatrix.Add(cell.X, cell.Y, cell);
        }

        /// <summary>
        ///     Возращает соседей клетки
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public IEnumerable<GolCell> GetNeighbors(GolCell cell)
        {
            IList<GolCell> neighbors = new List<GolCell>();
            AddNeighborCell(neighbors, GetNeighborCell(cell.X - 1, cell.Y - 1));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X, cell.Y - 1));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X + 1, cell.Y - 1));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X - 1, cell.Y));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X + 1, cell.Y));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X - 1, cell.Y + 1));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X, cell.Y + 1));
            AddNeighborCell(neighbors, GetNeighborCell(cell.X + 1, cell.Y + 1));
            return neighbors;
        }

        private void AddNeighborCell(IList<GolCell> neighbors, GolCell cell)
        {
            if (cell != null)
            {
                neighbors.Add(cell);
            }
        }

        public void IncrementGenerationNumber()
        {
            GenerationNumber++;
        }

        #endregion
    }
}