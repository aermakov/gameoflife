﻿using Newtonsoft.Json;

namespace GameOfLife.Core
{
    /// <summary>
    ///     Клетка
    /// </summary>
    public class GolCell
    {
        #region Fields

        #endregion

        #region Ctors

        public GolCell(ulong x, ulong y, GolCellState state)
        {
            X = x;
            Y = y;
            State = state;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Возвращает состояние следующего поколения
        /// </summary>
        [JsonIgnore]
        public GolCellState NextGenerationState { get; set; }

        /// <summary>
        ///     Возвращает текущее состояние
        /// </summary>
        [JsonIgnore]
        public GolCellState State { get; set; }

        /// <summary>
        ///     Возвращает X-координату клетки
        /// </summary>
        public ulong X { get; set; }

        /// <summary>
        ///     Возвращает Y-координату клетки
        /// </summary>
        public ulong Y { get; set; }

        /// <summary>
        ///     Возвращает значение, определяющее состояние клетки "мертвая"
        /// </summary>
        [JsonIgnore]
        public bool IsDead
        {
            get { return State == GolCellState.Dead; }
        }

        /// <summary>
        ///     Возвращает значение, определяющее состояние клетки "живая"
        /// </summary>
        [JsonIgnore]
        public bool IsAlive
        {
            get { return State == GolCellState.Alive; }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     "Воскрешение" клетки
        /// </summary>
        /// <param name="applyUpdate">признак обновления состояния клетки незамедлительно</param>
        public void Ret(bool applyUpdate = false)
        {
            NextGenerationState = GolCellState.Alive;

            if (applyUpdate)
            {
                State = NextGenerationState;
            }
        }

        /// <summary>
        ///     "Умервщление" клетки
        /// </summary>
        /// <param name="applyUpdate"></param>
        public void Die(bool applyUpdate = false)
        {
            NextGenerationState = GolCellState.Dead;

            if (applyUpdate)
            {
                State = NextGenerationState;
            }
        }

        protected bool Equals(GolCell other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((GolCell) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode()*397) ^ Y.GetHashCode();
            }
        }

        public override string ToString()
        {
            return new {State, X, Y}.ToString();
        }

        #endregion
    }
}