﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameOfLife.Core.Serialization;

namespace GameOfLife.Core.Storages
{
    public class FileSystemStorage : IStorage
    {
        public const string OUTPUT_DIRECTORY_KEY = "OutputDirectoryKey";

        public string OutputDirectory { get; set; }

        public void Configure(IDictionary<string, string> settings)
        {
            if (settings.ContainsKey(OUTPUT_DIRECTORY_KEY))
            {
                OutputDirectory = settings[OUTPUT_DIRECTORY_KEY];
            }
        }

        public void Save(IGolGridSerializer serializer, GolGrid grid, string name)
        {
            if (!string.IsNullOrEmpty(OutputDirectory))
            {
                if (!Directory.Exists(OutputDirectory))
                    Directory.CreateDirectory(OutputDirectory);

                string path = Path.Combine(OutputDirectory, string.Format("{0}.gol", name));
                using (TextWriter textWriter = new StreamWriter(path))
                {
                    serializer.Serialize(grid, textWriter);
                }
            }
        }

        public GolGrid Load(IGolGridSerializer serializer, string name)
        {
            GolGrid grid = null;
            if (!string.IsNullOrEmpty(OutputDirectory))
            {
                string path = Path.Combine(OutputDirectory, string.Format("{0}.gol", name));
                if (File.Exists(path))
                {
                    using (TextReader textReader = new StreamReader(path))
                    {
                        grid = serializer.Deserialize(textReader);
                    }
                }
            }
            return grid;
        }

        public IEnumerable<string> EnumerateNames()
        {
            if (!string.IsNullOrEmpty(OutputDirectory))
            {
                return Directory.EnumerateFiles(OutputDirectory).Select(Path.GetFileNameWithoutExtension);
            }
            return new string[] {};
        }
    }
}