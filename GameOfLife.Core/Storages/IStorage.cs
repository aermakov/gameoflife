﻿using System.Collections.Generic;
using GameOfLife.Core.Serialization;

namespace GameOfLife.Core.Storages
{
    public interface IStorage
    {
        void Configure(IDictionary<string, string> settings);
        void Save(IGolGridSerializer serializer, GolGrid grid, string name);
        GolGrid Load(IGolGridSerializer serializer, string name);
        IEnumerable<string> EnumerateNames();
    }
}