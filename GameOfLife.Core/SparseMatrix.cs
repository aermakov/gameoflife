﻿using System;
using System.Collections.Generic;

namespace GameOfLife.Core
{
    public class SparseMatrix<T> where T : class
    {
        private readonly Dictionary<Tuple<ulong, ulong>, T> _dictionary =
            new Dictionary<Tuple<ulong, ulong>, T>();

        public IEnumerator<T> GetEnumerator()
        {
            return _dictionary.Values.GetEnumerator();
        }

        public T Get(ulong x, ulong y)
        {
            Tuple<ulong, ulong> key = GetKey(x, y);
            T t = default(T);
            if (_dictionary.ContainsKey(key))
            {
                t = _dictionary[key];
            }
            return t;
        }

        public void Add(ulong x, ulong y, T t)
        {
            if (Equals(t, default(T)))
                throw new ArgumentNullException("t");

            Tuple<ulong, ulong> key = GetKey(x, y);
            if (!_dictionary.ContainsKey(key))
            {
                _dictionary.Add(key, t);
            }
        }

        public void Remove(ulong x, ulong y)
        {
            Tuple<ulong, ulong> key = GetKey(x, y);
            if (_dictionary.ContainsKey(key))
            {
                _dictionary.Remove(key);
            }
        }

        private Tuple<ulong, ulong> GetKey(ulong x, ulong y)
        {
            return new Tuple<ulong, ulong>(x, y);
        }

        public int Count()
        {
            return _dictionary.Count;
        }
    }
}