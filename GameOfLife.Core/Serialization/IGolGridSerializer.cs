﻿using System.IO;

namespace GameOfLife.Core.Serialization
{
    public interface IGolGridSerializer
    {
        void Serialize(GolGrid grid, TextWriter textWriter);
        GolGrid Deserialize(TextReader textReader);
    }
}