﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameOfLife.Core.Factories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GameOfLife.Core.Serialization
{
    public class JsonGolGridSerializer : IGolGridSerializer
    {
        public void Serialize(GolGrid grid, TextWriter textWriter)
        {
            var serializer = new JsonSerializer();
            using (JsonWriter jsonWriter = new JsonTextWriter(textWriter) {Formatting = Formatting.None})
            {
                serializer.Serialize(jsonWriter,
                    new
                    {
                        grid.Height,
                        grid.Width,
                        Cells = grid.GetAliveCells()
                    });
            }
        }

        public GolGrid Deserialize(TextReader textReader)
        {
            using (JsonReader jsonReader = new JsonTextReader(textReader))
            {
                JToken jToken = JToken.ReadFrom(jsonReader);

                var width = jToken.Value<ulong>("Width");
                var height = jToken.Value<ulong>("Height");
                var cellsArray = jToken.Value<JArray>("Cells");

                IEnumerable<Tuple<ulong, ulong>> cells = cellsArray
                    .Select(x => new Tuple<ulong, ulong>(x.Value<ulong>("X"), x.Value<ulong>("Y"))).ToArray();

                GolGrid grid = new GridFactory().CreateGrid(width, height,
                    cells.Select(x => new GolCell(x.Item1, x.Item2, GolCellState.Alive)));
                return grid;
            }
        }
    }
}